package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.StringUtils;
import com.huohuzhihui.oa.domain.OaAsset;
import com.huohuzhihui.oa.domain.OaCategory;
import com.huohuzhihui.oa.mapper.OaAssetMapper;
import com.huohuzhihui.oa.mapper.OaCategoryMapper;
import com.huohuzhihui.oa.service.IOaAssetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 资产信息Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaAssetServiceImpl implements IOaAssetService 
{
	private static final Logger log = LoggerFactory.getLogger(OaAssetServiceImpl.class);
    @Autowired
    private OaAssetMapper oaAssetMapper;
    @Autowired
    private OaCategoryMapper oaCategoryMapper;

    /**
     * 查询资产信息
     * 
     * @param id 资产信息ID
     * @return 资产信息
     */
    @Override
    public OaAsset selectOaAssetById(Long id)
    {
        return oaAssetMapper.selectOaAssetById(id);
    }

    /**
     * 查询资产信息列表
     * 
     * @param oaAsset 资产信息
     * @return 资产信息
     */
    @Override
    public List<OaAsset> selectOaAssetList(OaAsset oaAsset)
    {
        return oaAssetMapper.selectOaAssetList(oaAsset);
    }

    /**
     * 新增资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    @Override
    public int insertOaAsset(OaAsset oaAsset)
    {
    	oaAsset.setCreateBy(SecurityUtils.getUsername());
        oaAsset.setCreateTime(DateUtils.getNowDate());
        return oaAssetMapper.insertOaAsset(oaAsset);
    }

    /**
     * 修改资产信息
     * 
     * @param oaAsset 资产信息
     * @return 结果
     */
    @Override
    public int updateOaAsset(OaAsset oaAsset)
    {
    	oaAsset.setUpdateBy(SecurityUtils.getUsername());
        oaAsset.setUpdateTime(DateUtils.getNowDate());
        return oaAssetMapper.updateOaAsset(oaAsset);
    }

    /**
     * 批量删除资产信息
     * 
     * @param ids 需要删除的资产信息ID
     * @return 结果
     */
    @Override
    public int deleteOaAssetByIds(Long[] ids)
    {
        return oaAssetMapper.deleteOaAssetByIds(ids);
    }

    /**
     * 删除资产信息信息
     * 
     * @param id 资产信息ID
     * @return 结果
     */
    @Override
    public int deleteOaAssetById(Long id)
    {
        return oaAssetMapper.deleteOaAssetById(id);
    }

    /**
     * 导入资产数据
     * 
     * @param list List<OaAsset> 资产数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    @Transactional
    public String imports(List<OaAsset> list, Boolean isUpdateSupport, String operName) {
    	if (StringUtils.isNull(list) || list.size() == 0)
        {
            throw new CustomException("导入资产不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (OaAsset vo : list)
        {
            try
            {
         	   OaCategory sc = new OaCategory();
         	   sc.setTitle(vo.getCateName());
         	  OaCategory s = oaCategoryMapper.findOaCategory(sc);
         	   if(s == null) {
         		   throw new CustomException("资产分类 "+vo.getCateName()+" 的信息不存在");
         	   }
         	   vo.setCateId(s.getId());

         	    
         	  OaAsset oc = new  OaAsset();
         	   oc.setCode(vo.getCode());
         	  OaAsset old = oaAssetMapper.findOaAsset(oc);
	               	            
                if (StringUtils.isNull(old))
                {
                    this.insertOaAsset(vo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、 资产 " + vo.getTitle() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    vo.setId(old.getId());
                    this.updateOaAsset(vo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、 资产 " + vo.getTitle() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、 资产 " + vo.getTitle() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、 资产 " + vo.getTitle() + "导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
