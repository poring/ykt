package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaReport;

import java.util.List;

/**
 * 工作汇报Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaReportService 
{
    /**
     * 查询工作汇报
     * 
     * @param id 工作汇报ID
     * @return 工作汇报
     */
    public OaReport selectOaReportById(Long id);

    /**
     * 查询工作汇报列表
     * 
     * @param oaReport 工作汇报
     * @return 工作汇报集合
     */
    public List<OaReport> selectOaReportList(OaReport oaReport);

    /**
     * 新增工作汇报
     * 
     * @param oaReport 工作汇报
     * @return 结果
     */
    public int insertOaReport(OaReport oaReport);

    /**
     * 修改工作汇报
     * 
     * @param oaReport 工作汇报
     * @return 结果
     */
    public int updateOaReport(OaReport oaReport);

    /**
     * 批量删除工作汇报
     * 
     * @param ids 需要删除的工作汇报ID
     * @return 结果
     */
    public int deleteOaReportByIds(Long[] ids);

    /**
     * 删除工作汇报信息
     * 
     * @param id 工作汇报ID
     * @return 结果
     */
    public int deleteOaReportById(Long id);
}
