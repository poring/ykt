package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaDivan;
import com.huohuzhihui.oa.mapper.OaDivanMapper;
import com.huohuzhihui.oa.service.IOaDivanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会议室Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaDivanServiceImpl implements IOaDivanService 
{
    @Autowired
    private OaDivanMapper oaDivanMapper;

    /**
     * 查询会议室
     * 
     * @param id 会议室ID
     * @return 会议室
     */
    @Override
    public OaDivan selectOaDivanById(Long id)
    {
        return oaDivanMapper.selectOaDivanById(id);
    }

    /**
     * 查询会议室列表
     * 
     * @param oaDivan 会议室
     * @return 会议室
     */
    @Override
    public List<OaDivan> selectOaDivanList(OaDivan oaDivan)
    {
        return oaDivanMapper.selectOaDivanList(oaDivan);
    }

    /**
     * 新增会议室
     * 
     * @param oaDivan 会议室
     * @return 结果
     */
    @Override
    public int insertOaDivan(OaDivan oaDivan)
    {
    	oaDivan.setCreateBy(SecurityUtils.getUsername());
        oaDivan.setCreateTime(DateUtils.getNowDate());
        return oaDivanMapper.insertOaDivan(oaDivan);
    }

    /**
     * 修改会议室
     * 
     * @param oaDivan 会议室
     * @return 结果
     */
    @Override
    public int updateOaDivan(OaDivan oaDivan)
    {
    	oaDivan.setUpdateBy(SecurityUtils.getUsername());
        oaDivan.setUpdateTime(DateUtils.getNowDate());
        return oaDivanMapper.updateOaDivan(oaDivan);
    }

    /**
     * 批量删除会议室
     * 
     * @param ids 需要删除的会议室ID
     * @return 结果
     */
    @Override
    public int deleteOaDivanByIds(Long[] ids)
    {
        return oaDivanMapper.deleteOaDivanByIds(ids);
    }

    /**
     * 删除会议室信息
     * 
     * @param id 会议室ID
     * @return 结果
     */
    @Override
    public int deleteOaDivanById(Long id)
    {
        return oaDivanMapper.deleteOaDivanById(id);
    }
}
