package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaArchive;
import com.huohuzhihui.oa.domain.OaArchiveRead;
import com.huohuzhihui.oa.mapper.OaArchiveMapper;
import com.huohuzhihui.oa.mapper.OaArchiveReadMapper;
import com.huohuzhihui.oa.service.IOaArchiveService;
import com.huohuzhihui.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 文档轮阅Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-15
 */
@Service
public class OaArchiveServiceImpl implements IOaArchiveService 
{
    @Autowired
    private OaArchiveMapper oaArchiveMapper;
    @Autowired
    private OaArchiveReadMapper oaArchiveReadMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询文档轮阅
     * 
     * @param id 文档轮阅ID
     * @return 文档轮阅
     */
    @Override
    public OaArchive selectOaArchiveById(Long id)
    {
    	OaArchive archive = oaArchiveMapper.selectOaArchiveById(id);
    	OaArchiveRead rc = new OaArchiveRead();
    	rc.setArchiveId(id);
    	List<OaArchiveRead> list = oaArchiveReadMapper.selectOaArchiveReadList(rc);
    	if(list != null && list.size() > 0) {
    		Long users[] = new Long[list.size()];
    		String userNames = "";
    		for(int i=0;i<list.size();i++) {
    			users[i] = list.get(i).getUserId();
    			userNames += "," + list.get(i).getName();
    		}
    		archive.setUsers(users);
    		archive.setUserNames(userNames.substring(1));
    	}
    	return archive;
    }

    /**
     * 查询文档轮阅列表
     * 
     * @param oaArchive 文档轮阅
     * @return 文档轮阅
     */
    @Override
    public List<OaArchive> selectOaArchiveList(OaArchive oaArchive)
    {
        return oaArchiveMapper.selectOaArchiveList(oaArchive);
    }

    /**
     * 新增文档轮阅
     * 
     * @param oaArchive 文档轮阅
     * @return 结果
     */
    @Override
    @Transactional
    public int insertOaArchive(OaArchive oaArchive)
    {
        oaArchive.setAddTime(DateUtils.getNowDate());
        oaArchive.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
        oaArchive.setStatus("N");
        int count = oaArchiveMapper.insertOaArchive(oaArchive);
        if(oaArchive.getUsers() != null || oaArchive.getUsers().length > 0) {
        	for(Long userId : oaArchive.getUsers()) {
        		SysUser u = sysUserMapper.selectUserById(userId);
        		OaArchiveRead read = new OaArchiveRead();
        		read.setUserId(userId);
        		read.setArchiveId(oaArchive.getId());
        		read.setName(u.getNickName());
        		oaArchiveReadMapper.insertOaArchiveRead(read);
        		count ++;
        	}
        }
        return count;
    }

    /**
     * 修改文档轮阅
     * 
     * @param oaArchive 文档轮阅
     * @return 结果
     */
    @Override
    public int updateOaArchive(OaArchive oaArchive)
    {
    	OaArchiveRead rc = new OaArchiveRead();
    	rc.setArchiveId(oaArchive.getId());
    	List<OaArchiveRead> list = oaArchiveReadMapper.selectOaArchiveReadList(rc);
    	Set<Long> users = new HashSet<>();
    	for(Long userId : oaArchive.getUsers()) {
    		users.add(userId);
    	}
		for(OaArchiveRead read:list) {
			if(users.contains(read.getUserId())) {
				users.remove(read.getUserId());
			}else if(read.getReadTime() == null) {
				oaArchiveReadMapper.deleteOaArchiveReadById(read.getId());
			}
		}
		for(Long userId : users) {
    		SysUser u = sysUserMapper.selectUserById(userId);
    		OaArchiveRead read = new OaArchiveRead();
    		read.setUserId(userId);
    		read.setArchiveId(oaArchive.getId());
    		read.setName(u.getNickName());
    		oaArchiveReadMapper.insertOaArchiveRead(read);
    	}
        return oaArchiveMapper.updateOaArchive(oaArchive);
    }

    /**
     * 批量删除文档轮阅
     * 
     * @param ids 需要删除的文档轮阅ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteOaArchiveByIds(Long[] ids)
    {
    	for(Long id:ids) {
        	OaArchiveRead rc = new OaArchiveRead();
        	rc.setArchiveId(id);
        	List<OaArchiveRead> list = oaArchiveReadMapper.selectOaArchiveReadList(rc);
        	for(OaArchiveRead read:list) {
        		if(read.getReadTime() != null) {
        			throw new CustomException("已经阅读的文件不能删除");
        		}
        		oaArchiveReadMapper.deleteOaArchiveReadById(read.getId());
        	}
    	}
        return oaArchiveMapper.deleteOaArchiveByIds(ids);
    }

    /**
     * 删除文档轮阅信息
     * 
     * @param id 文档轮阅ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteOaArchiveById(Long id)
    {
    	OaArchiveRead rc = new OaArchiveRead();
    	rc.setArchiveId(id);
    	List<OaArchiveRead> list = oaArchiveReadMapper.selectOaArchiveReadList(rc);
    	for(OaArchiveRead read:list) {
    		if(read.getReadTime() != null) {
    			throw new CustomException("已经阅读的文件不能删除");
    		}
    		oaArchiveReadMapper.deleteOaArchiveReadById(read.getId());
    	}
        return oaArchiveMapper.deleteOaArchiveById(id);
    }
}
