package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaMeetingUser;

import java.util.List;

/**
 * 与会人员Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaMeetingUserService 
{
    /**
     * 查询与会人员
     * 
     * @param id 与会人员ID
     * @return 与会人员
     */
    public OaMeetingUser selectOaMeetingUserById(Long id);

    /**
     * 查询与会人员列表
     * 
     * @param oaMeetingUser 与会人员
     * @return 与会人员集合
     */
    public List<OaMeetingUser> selectOaMeetingUserList(OaMeetingUser oaMeetingUser);

    /**
     * 新增与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    public int insertOaMeetingUser(OaMeetingUser oaMeetingUser);

    /**
     * 修改与会人员
     * 
     * @param oaMeetingUser 与会人员
     * @return 结果
     */
    public int updateOaMeetingUser(OaMeetingUser oaMeetingUser);

    /**
     * 批量删除与会人员
     * 
     * @param ids 需要删除的与会人员ID
     * @return 结果
     */
    public int deleteOaMeetingUserByIds(Long[] ids);

    /**
     * 删除与会人员信息
     * 
     * @param id 与会人员ID
     * @return 结果
     */
    public int deleteOaMeetingUserById(Long id);
}
