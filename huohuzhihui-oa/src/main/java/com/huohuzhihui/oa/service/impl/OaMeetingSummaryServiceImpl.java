package com.huohuzhihui.oa.service.impl;

import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.oa.domain.OaMeetingSummary;
import com.huohuzhihui.oa.mapper.OaMeetingSummaryMapper;
import com.huohuzhihui.oa.service.IOaMeetingSummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会议记要Service业务层处理
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Service
public class OaMeetingSummaryServiceImpl implements IOaMeetingSummaryService 
{
    @Autowired
    private OaMeetingSummaryMapper oaMeetingSummaryMapper;

    /**
     * 查询会议记要
     * 
     * @param id 会议记要ID
     * @return 会议记要
     */
    @Override
    public OaMeetingSummary selectOaMeetingSummaryById(Long id)
    {
        return oaMeetingSummaryMapper.selectOaMeetingSummaryById(id);
    }

    /**
     * 查询会议记要列表
     * 
     * @param oaMeetingSummary 会议记要
     * @return 会议记要
     */
    @Override
    public List<OaMeetingSummary> selectOaMeetingSummaryList(OaMeetingSummary oaMeetingSummary)
    {
        return oaMeetingSummaryMapper.selectOaMeetingSummaryList(oaMeetingSummary);
    }

    /**
     * 新增会议记要
     * 
     * @param oaMeetingSummary 会议记要
     * @return 结果
     */
    @Override
    public int insertOaMeetingSummary(OaMeetingSummary oaMeetingSummary)
    {
        oaMeetingSummary.setCreateTime(DateUtils.getNowDate());
        oaMeetingSummary.setCreateBy(SecurityUtils.getUsername());
        return oaMeetingSummaryMapper.insertOaMeetingSummary(oaMeetingSummary);
    }

    /**
     * 修改会议记要
     * 
     * @param oaMeetingSummary 会议记要
     * @return 结果
     */
    @Override
    public int updateOaMeetingSummary(OaMeetingSummary oaMeetingSummary)
    {
        return oaMeetingSummaryMapper.updateOaMeetingSummary(oaMeetingSummary);
    }

    /**
     * 批量删除会议记要
     * 
     * @param ids 需要删除的会议记要ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingSummaryByIds(Long[] ids)
    {
        return oaMeetingSummaryMapper.deleteOaMeetingSummaryByIds(ids);
    }

    /**
     * 删除会议记要信息
     * 
     * @param id 会议记要ID
     * @return 结果
     */
    @Override
    public int deleteOaMeetingSummaryById(Long id)
    {
        return oaMeetingSummaryMapper.deleteOaMeetingSummaryById(id);
    }
}
