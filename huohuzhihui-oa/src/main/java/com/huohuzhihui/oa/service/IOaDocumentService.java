package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaDocument;

import java.util.List;

/**
 * 公文Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaDocumentService 
{
    /**
     * 查询公文
     * 
     * @param id 公文ID
     * @return 公文
     */
    public OaDocument selectOaDocumentById(Long id);

    /**
     * 查询公文列表
     * 
     * @param oaDocument 公文
     * @return 公文集合
     */
    public List<OaDocument> selectOaDocumentList(OaDocument oaDocument);

    /**
     * 新增公文
     * 
     * @param oaDocument 公文
     * @return 结果
     */
    public int insertOaDocument(OaDocument oaDocument);

    /**
     * 修改公文
     * 
     * @param oaDocument 公文
     * @return 结果
     */
    public int updateOaDocument(OaDocument oaDocument);

    /**
     * 批量删除公文
     * 
     * @param ids 需要删除的公文ID
     * @return 结果
     */
    public int deleteOaDocumentByIds(Long[] ids);

    /**
     * 删除公文信息
     * 
     * @param id 公文ID
     * @return 结果
     */
    public int deleteOaDocumentById(Long id);
    
    /**
     * 撤消公文信息
     * 
     * @param id 公文ID
     * @return 结果
     */
    public int rollbackOaDocumentById(Long id);
    
    /**
     * 发送公文
     * 
     * @param id 公文ID
     * @return 公文
     */
    public int sendOaDocument(Long id);

}
