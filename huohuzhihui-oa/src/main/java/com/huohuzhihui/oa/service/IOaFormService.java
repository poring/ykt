package com.huohuzhihui.oa.service;

import com.huohuzhihui.oa.domain.OaForm;

import java.util.List;

/**
 * 公文单Service接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface IOaFormService 
{
    /**
     * 查询公文单
     * 
     * @param id 公文单ID
     * @return 公文单
     */
    public OaForm selectOaFormById(Long id);

    /**
     * 查询公文单列表
     * 
     * @param oaForm 公文单
     * @return 公文单集合
     */
    public List<OaForm> selectOaFormList(OaForm oaForm);

    /**
     * 新增公文单
     * 
     * @param oaForm 公文单
     * @return 结果
     */
    public int insertOaForm(OaForm oaForm);

    /**
     * 修改公文单
     * 
     * @param oaForm 公文单
     * @return 结果
     */
    public int updateOaForm(OaForm oaForm);

    /**
     * 批量删除公文单
     * 
     * @param ids 需要删除的公文单ID
     * @return 结果
     */
    public int deleteOaFormByIds(Long[] ids);

    /**
     * 删除公文单信息
     * 
     * @param id 公文单ID
     * @return 结果
     */
    public int deleteOaFormById(Long id);
}
