package com.huohuzhihui.oa.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 会议申请对象 oa_meeting
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("会议申请实体")
@Getter
@Setter
public class OaMeeting extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;
    private Long batchs[];
    private Long users[];
    private String userNames;

    /** 会议名称 */
    @Excel(name = "会议名称")
    @ApiModelProperty("会议名称")
    private String title;

    /** 会议室 */
    @ApiModelProperty("会议室")
    private Long divanId;
    @Excel(name = "会议室")
    private String divanName;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty("联系人")
    private String linkman;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty("联系电话")
    private String phone;

    /** 地址 */
    @Excel(name = "地址")
    @ApiModelProperty("地址")
    private String address;

    /** 开始使用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始使用时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("开始使用时间")
    private Date startTime;

    /** 结束使用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束使用时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("结束使用时间")
    private Date finishTime;

    /** 会议内容 */
    @Excel(name = "会议内容")
    @ApiModelProperty("会议内容")
    private String content;

    /** 申请说明 */
    @Excel(name = "申请说明")
    @ApiModelProperty("申请说明")
    private String comment;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("申请时间")
    private Date addTime;

    /** 申请人 */
    @ApiModelProperty("申请人")
    private Long addUser;
    @Excel(name = "申请人")
    private String addUserName;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date checkTime;

    /** 审核人 */
    private Long checkUser;
    private String checkUserName;

    /** 审核状态 */
    private String checkStatus;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("divanId", getDivanId())
            .append("linkman", getLinkman())
            .append("phone", getPhone())
            .append("address", getAddress())
            .append("startTime", getStartTime())
            .append("finishTime", getFinishTime())
            .append("addTime", getAddTime())
            .append("addUser", getAddUser())
            .append("checkTime", getCheckTime())
            .append("checkUser", getCheckUser())
            .append("checkStatus", getCheckStatus())
            .toString();
    }
}
