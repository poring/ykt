package com.huohuzhihui.oa.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.TreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 资产分类对象 oa_category
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@ApiModel("资产分类实体")
@Getter
@Setter
public class OaCategory extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增长主键ID */
    private Long id;

    /** 上级 */
    @Excel(name = "上级")
    @ApiModelProperty("上级")
    private Long pid;

    /** 分类名称 */
    @Excel(name = "分类名称")
    @ApiModelProperty("分类名称")
    private String title;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty("排序")
    private Long listSort;



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pid", getPid())
            .append("title", getTitle())
            .append("listSort", getListSort())
            .toString();
    }
}
