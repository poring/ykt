package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaReport;
import com.huohuzhihui.oa.service.IOaReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作汇报Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("工作汇报信息管理")
@RestController
@RequestMapping("/oa/report")
public class OaReportController extends BaseController
{
    @Autowired
    private IOaReportService oaReportService;

    /**
     * 查询工作汇报列表
     */
    @ApiOperation("获取工作汇报列表")
    @PreAuthorize("@ss.hasPermi('oa:report:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaReport oaReport)
    {
        startPage();
        List<OaReport> list = oaReportService.selectOaReportList(oaReport);
        return getDataTable(list);
    }
    
    /**
     * 查询个人工作汇报列表
     */
    @ApiOperation("获取个人工作汇报列表")
    @PreAuthorize("@ss.hasPermi('oa:report:my')")
    @GetMapping("/my")
    public TableDataInfo my(OaReport oaReport)
    {
        startPage();
        oaReport.setAddUser(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaReport> list = oaReportService.selectOaReportList(oaReport);
        return getDataTable(list);
    }
    

    /**
     * 查询待查阅个人工作汇报列表
     */
    @ApiOperation("获取待查阅工作汇报列表")
    @PreAuthorize("@ss.hasPermi('oa:report:list')")
    @GetMapping("/toto")
    public TableDataInfo toto(OaReport oaReport)
    {
        startPage();
        oaReport.setToUser(SecurityUtils.getLoginUser().getUser().getUserId());
        List<OaReport> list = oaReportService.selectOaReportList(oaReport);
        return getDataTable(list);
    }

    /**
     * 导出工作汇报列表
     */
    @ApiOperation("导出工作汇报列表")
    @PreAuthorize("@ss.hasPermi('oa:report:export')")
    @Log(title = "工作汇报", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaReport oaReport)
    {
        List<OaReport> list = oaReportService.selectOaReportList(oaReport);
        ExcelUtil<OaReport> util = new ExcelUtil<OaReport>(OaReport.class);
        return util.exportExcel(list, "工作汇报");
    }

    /**
     * 获取工作汇报详细信息
     */
    @ApiOperation("获取工作汇报详细信息")
    @ApiImplicitParam(name = "id", value = "工作汇报ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:report:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaReportService.selectOaReportById(id));
    }

    /**
     * 新增工作汇报
     */
    @ApiOperation("新增工作汇报")
    @PreAuthorize("@ss.hasPermi('oa:report:add')")
    @Log(title = "工作汇报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaReport oaReport)
    {
        return toAjax(oaReportService.insertOaReport(oaReport));
    }

    /**
     * 修改工作汇报
     */
    @ApiOperation("修改工作汇报")
    @PreAuthorize("@ss.hasPermi('oa:report:edit')")
    @Log(title = "工作汇报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaReport oaReport)
    {
        return toAjax(oaReportService.updateOaReport(oaReport));
    }

    /**
     * 删除工作汇报
     */
    @ApiOperation("删除工作汇报")
    @ApiImplicitParam(name = "id", value = "工作汇报ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:report:remove')")
    @Log(title = "工作汇报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaReportService.deleteOaReportByIds(ids));
    }
}
