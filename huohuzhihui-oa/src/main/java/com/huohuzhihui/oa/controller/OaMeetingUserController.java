package com.huohuzhihui.oa.controller;

import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.page.TableDataInfo;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.oa.domain.OaMeetingUser;
import com.huohuzhihui.oa.service.IOaMeetingUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 与会人员Controller
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
@Api("与会人员信息管理")
@RestController
@RequestMapping("/oa/user")
public class OaMeetingUserController extends BaseController
{
    @Autowired
    private IOaMeetingUserService oaMeetingUserService;

    /**
     * 查询与会人员列表
     */
    @ApiOperation("获取与会人员列表")
    @PreAuthorize("@ss.hasPermi('oa:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(OaMeetingUser oaMeetingUser)
    {
        startPage();
        List<OaMeetingUser> list = oaMeetingUserService.selectOaMeetingUserList(oaMeetingUser);
        return getDataTable(list);
    }

    /**
     * 导出与会人员列表
     */
    @ApiOperation("导出与会人员列表")
    @PreAuthorize("@ss.hasPermi('oa:user:export')")
    @Log(title = "与会人员", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OaMeetingUser oaMeetingUser)
    {
        List<OaMeetingUser> list = oaMeetingUserService.selectOaMeetingUserList(oaMeetingUser);
        ExcelUtil<OaMeetingUser> util = new ExcelUtil<OaMeetingUser>(OaMeetingUser.class);
        return util.exportExcel(list, "与会人员");
    }

    /**
     * 获取与会人员详细信息
     */
    @ApiOperation("获取与会人员详细信息")
    @ApiImplicitParam(name = "id", value = "与会人员ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(oaMeetingUserService.selectOaMeetingUserById(id));
    }

    /**
     * 新增与会人员
     */
    @ApiOperation("新增与会人员")
    @PreAuthorize("@ss.hasPermi('oa:user:add')")
    @Log(title = "与会人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OaMeetingUser oaMeetingUser)
    {
        return toAjax(oaMeetingUserService.insertOaMeetingUser(oaMeetingUser));
    }

    /**
     * 修改与会人员
     */
    @ApiOperation("修改与会人员")
    @PreAuthorize("@ss.hasPermi('oa:user:edit')")
    @Log(title = "与会人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OaMeetingUser oaMeetingUser)
    {
        return toAjax(oaMeetingUserService.updateOaMeetingUser(oaMeetingUser));
    }

    /**
     * 删除与会人员
     */
    @ApiOperation("删除与会人员")
    @ApiImplicitParam(name = "id", value = "与会人员ID", required = true, dataType = "int", paramType = "path")
    @PreAuthorize("@ss.hasPermi('oa:user:remove')")
    @Log(title = "与会人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(oaMeetingUserService.deleteOaMeetingUserByIds(ids));
    }
}
