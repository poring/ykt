package com.huohuzhihui.oa.mapper;

import com.huohuzhihui.oa.domain.OaGuard;

import java.util.List;

/**
 * 门禁Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-08
 */
public interface OaGuardMapper 
{
    /**
     * 查询门禁
     * 
     * @param id 门禁ID
     * @return 门禁
     */
    public OaGuard selectOaGuardById(Long id);

    /**
     * 查询门禁列表
     * 
     * @param oaGuard 门禁
     * @return 门禁集合
     */
    public List<OaGuard> selectOaGuardList(OaGuard oaGuard);

    /**
     * 新增门禁
     * 
     * @param oaGuard 门禁
     * @return 结果
     */
    public int insertOaGuard(OaGuard oaGuard);

    /**
     * 修改门禁
     * 
     * @param oaGuard 门禁
     * @return 结果
     */
    public int updateOaGuard(OaGuard oaGuard);

    /**
     * 删除门禁
     * 
     * @param id 门禁ID
     * @return 结果
     */
    public int deleteOaGuardById(Long id);

    /**
     * 批量删除门禁
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaGuardByIds(Long[] ids);
}
