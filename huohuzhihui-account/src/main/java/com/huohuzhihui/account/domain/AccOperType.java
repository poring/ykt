package com.huohuzhihui.account.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 账户操作类型对象 acc_oper_type
 * 
 * @author huohuzhihui
 * @date 2021-08-25
 */
public class AccOperType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 操作名称 */
    @Excel(name = "操作名称")
    private String name;

    /** 余额变化：－1:余额减少,1:余额增加,0:余额不发生变化 */
    @Excel(name = "余额变化：－1:余额减少,1:余额增加,0:余额不发生变化")
    private Integer balanceChange;

    /** 0：启用；1：禁用 */
    @Excel(name = "0：启用；1：禁用")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setBalanceChange(Integer balanceChange) 
    {
        this.balanceChange = balanceChange;
    }

    public Integer getBalanceChange() 
    {
        return balanceChange;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("balanceChange", getBalanceChange())
            .append("status", getStatus())
            .toString();
    }
}
