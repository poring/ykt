package com.huohuzhihui.door.service.impl;

import com.huohuzhihui.account.service.IAccCardService;
import com.huohuzhihui.common.core.domain.entity.Card;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.door.domain.Door;
import com.huohuzhihui.door.domain.DoorAuth;
import com.huohuzhihui.door.domain.DoorDevice;
import com.huohuzhihui.door.mapper.DoorAuthMapper;
import com.huohuzhihui.door.service.IDoorAuthService;
import com.huohuzhihui.door.service.IDoorDeviceService;
import com.huohuzhihui.door.service.IDoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;


/**
 * 门禁授权Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
@Service
public class DoorAuthServiceImpl implements IDoorAuthService 
{
    @Autowired
    private DoorAuthMapper doorAuthMapper;

    @Autowired
    private IAccCardService accCardService;

    @Autowired
    private WgDoorDeviceOperServiceImpl wgDoorDeviceOper;

    @Autowired
    private IDoorService doorService;

    @Autowired
    private IDoorDeviceService doorDeviceService;
    /**
     * 查询门禁授权
     *
     * @param id 门禁授权ID
     * @return 门禁授权
     */
    @Override
    public DoorAuth selectDoorAuthById(Long id)
    {
        return doorAuthMapper.selectDoorAuthById(id);
    }

    /**
     * 查询门禁授权列表
     *
     *
     * @param doorAuth 门禁授权
     * @return 门禁授权
     */
    @Override
    public List<DoorAuth> selectDoorAuthList(DoorAuth doorAuth)
    {
        return doorAuthMapper.selectDoorAuthList(doorAuth);
    }

    /**
     * 新增门禁授权
     *
     *
     * @param doorAuth 门禁授权
     * @return 结果
     */
    @Override
    public int insertDoorAuth(DoorAuth doorAuth)
    {
        int ret = 0;
                //查询用户卡号
        List<Card> cardList = accCardService.selectAccCardByUserId(doorAuth.getUserId());
        Long cardNo = 0L;
        if(cardList!=null && cardList.size()>0){
            for(int i = 0 ; i < cardList.size();i++){
                Card card = cardList.get(i);
                if(card.getStatus()==0){
                    cardNo = card.getCardNo();
                    break;
                }
            }
            doorAuth.setCardNo(cardNo);
            doorAuth.setCreateTime(DateUtils.getNowDate());
            ret = doorAuthMapper.insertDoorAuth(doorAuth);

            int writeSuccess = writeAuth(doorAuth);
            if(writeSuccess==1){
                doorAuth.setIsWrited(1);
                doorAuthMapper.updateDoorAuth(doorAuth);
            }
            return ret;
        }
        return ret;
    }

    /**
     * 同步权限到设备
     * @param doorAuth
     * @return 同步结果：1为成功一条
     */
    @Override
    public int writeAuth(DoorAuth doorAuth) {
        //查询门
        long doorId = doorAuth.getDoorId();
        Door door = this.doorService.selectDoorById(doorId);
        int lockNo = door.getLockNo();

        DoorDevice doorDevice = doorDeviceService.selectDoorDeviceById(door.getDeviceId());
        String ip = doorDevice.getDeviceIp();
        String sn = doorDevice.getDeviceSn();
        int port = doorDevice.getDevicePort();
        long cardNo = doorAuth.getCardNo();

        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String startTime = df.format(doorAuth.getStartDate());
        String endTime = df.format(doorAuth.getEndDate());

        return wgDoorDeviceOper.writeAuth( sn, ip, port, lockNo, cardNo, startTime, endTime);
    }

    /**
     * 修改门禁授权
     *
     *
     * @param doorAuth 门禁授权
     * @return 结果
     */
    @Override
    public int updateDoorAuth(DoorAuth doorAuth)
    {
        doorAuth.setUpdateTime(DateUtils.getNowDate());
        return doorAuthMapper.updateDoorAuth(doorAuth);
    }

    /**
     * 批量删除门禁授权
     *
     *
     * @param ids 需要删除的门禁授权ID
     * @return 结果
     */
    @Override
    public int deleteDoorAuthByIds(String[] ids)
    {
        return doorAuthMapper.deleteDoorAuthByIds(ids);
    }
}
