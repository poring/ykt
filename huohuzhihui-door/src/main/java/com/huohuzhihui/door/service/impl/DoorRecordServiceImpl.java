package com.huohuzhihui.door.service.impl;

import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.door.domain.Door;
import com.huohuzhihui.door.domain.DoorAuth;
import com.huohuzhihui.door.domain.DoorDevice;
import com.huohuzhihui.door.domain.DoorRecord;
import com.huohuzhihui.door.mapper.DoorRecordMapper;
import com.huohuzhihui.door.service.IDoorAuthService;
import com.huohuzhihui.door.service.IDoorDeviceOperService;
import com.huohuzhihui.door.service.IDoorDeviceService;
import com.huohuzhihui.door.service.IDoorRecordService;
import com.huohuzhihui.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 门禁记录Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-22
 */
@Service
public class DoorRecordServiceImpl implements IDoorRecordService 
{
    @Autowired
    private DoorRecordMapper doorRecordMapper;
    @Autowired
    private IDoorDeviceOperService wgDoorDeviceOperService;
    @Autowired
    private IDoorDeviceService doorDeviceService;
    @Autowired
    private IDoorAuthService doorAuthService;
    @Autowired
    private ISysUserService sysUserService;
    /**
     * 查询门禁记录
     * 
     * @param id 门禁记录ID
     * @return 门禁记录
     */
    @Override
    public DoorRecord selectDoorRecordById(Long id)
    {
        return doorRecordMapper.selectDoorRecordById(id);
    }

    /**
     * 查询门禁记录列表
     * 
     * @param doorRecord 门禁记录
     * @return 门禁记录
     */
    @Override
    public List<DoorRecord> selectDoorRecordList(DoorRecord doorRecord)
    {
        return doorRecordMapper.selectDoorRecordList(doorRecord);
    }

    /**
     * 新增门禁记录
     * 
     * @param doorRecord 门禁记录
     * @return 结果
     */
    @Override
    public int insertDoorRecord(DoorRecord doorRecord)
    {
        doorRecord.setCreateTime(DateUtils.getNowDate());
        return doorRecordMapper.insertDoorRecord(doorRecord);
    }

    /**
     * 修改门禁记录
     * 
     * @param doorRecord 门禁记录
     * @return 结果
     */
    @Override
    public int updateDoorRecord(DoorRecord doorRecord)
    {
        return doorRecordMapper.updateDoorRecord(doorRecord);
    }

    /**
     * 批量删除门禁记录
     * 
     * @param ids 需要删除的门禁记录ID
     * @return 结果
     */
    @Override
    public int deleteDoorRecordByIds(String[] ids)
    {
        return doorRecordMapper.deleteDoorRecordByIds(ids);
    }


    @Override
    public void device2local(Door door)throws CustomException {
        int lockNo = door.getLockNo();

        DoorDevice doorDevice = doorDeviceService.selectDoorDeviceById(door.getDeviceId());
        String ip = doorDevice.getDeviceIp();
        String sn = doorDevice.getDeviceSn();
        int port = doorDevice.getDevicePort();

        List<DoorRecord> doorRecordList = wgDoorDeviceOperService.findInoutRecord(sn,ip,port,lockNo);
        doorRecordList.forEach(r->{
                String deviceCardNo = r.getCardNo();
                DoorAuth doorAuth = new DoorAuth();
                doorAuth.setDeviceCardNo(deviceCardNo);
                List<DoorAuth> doorAuthList = doorAuthService.selectDoorAuthList(doorAuth);
                if(doorAuthList!=null && doorAuthList.size()>0){
                    DoorAuth auth = doorAuthList.get(0);
                    long userId = auth.getUserId();
                    SysUser user = sysUserService.selectUserById(userId);

                    //保存记录到数据库
                    //先去重
                    if(!isExist(r)){
                        this.insertDoorRecord(r);
                    }
                }
            }
        );
    }

    /**
     * 根据开门的卡号和时间判断进出记录是否已经保存在了数据库
     * @param doorRecord
     * @return
     */
    private boolean isExist(DoorRecord doorRecord){
        DoorRecord record = new DoorRecord();
        record.setOpenTime(doorRecord.getOpenTime());
        record.setCardNo(doorRecord.getCardNo());
        List<DoorRecord> doorRecordList = selectDoorRecordList(record);
        if(doorRecordList==null || doorRecordList.size()==0){
            return false;
        }else{
            return true;
        }
    }



}
