package com.huohuzhihui.door.service.impl;

import java.util.List;

import com.huohuzhihui.account.service.IAccCardService;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.door.mapper.DoorDeviceMapper;
import com.huohuzhihui.door.domain.DoorDevice;
import com.huohuzhihui.door.service.IDoorDeviceService;

/**
 * 门禁设备Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2021-08-09
 */
@Service
public class DoorDeviceServiceImpl implements IDoorDeviceService 
{
    @Autowired
    private DoorDeviceMapper doorDeviceMapper;



    /**
     * 查询门禁设备
     * 
     * @param id 门禁设备ID
     * @return 门禁设备
     */
    @Override
    public DoorDevice selectDoorDeviceById(Long id)
    {
        return doorDeviceMapper.selectDoorDeviceById(id);
    }

    /**
     * 查询门禁设备列表
     * 
     * @param doorDevice 门禁设备
     * @return 门禁设备
     */
    @Override
    public List<DoorDevice> selectDoorDeviceList(DoorDevice doorDevice)
    {
        return doorDeviceMapper.selectDoorDeviceList(doorDevice);
    }

    /**
     * 新增门禁设备
     * 
     * @param doorDevice 门禁设备
     * @return 结果
     */
    @Override
    public int insertDoorDevice(DoorDevice doorDevice)
    {
        doorDevice.setCreateTime(DateUtils.getNowDate());
        return doorDeviceMapper.insertDoorDevice(doorDevice);


    }

    /**
     * 修改门禁设备
     * 
     * @param doorDevice 门禁设备
     * @return 结果
     */
    @Override
    public int updateDoorDevice(DoorDevice doorDevice)
    {
        doorDevice.setUpdateTime(DateUtils.getNowDate());
        return doorDeviceMapper.updateDoorDevice(doorDevice);
    }

    /**
     * 批量删除门禁设备
     * 
     * @param ids 需要删除的门禁设备ID
     * @return 结果
     */
    @Override
    public int deleteDoorDeviceByIds(Long[] ids)
    {
        return doorDeviceMapper.deleteDoorDeviceByIds(ids);
    }

    /**
     * 删除门禁设备信息
     * 
     * @param id 门禁设备ID
     * @return 结果
     */
    @Override
    public int deleteDoorDeviceById(Long id)
    {
        return doorDeviceMapper.deleteDoorDeviceById(id);
    }



}
