import request from '@/utils/request'

// 查询工作汇报列表
export function listReport(query) {
  return request({
    url: '/oa/report/list',
    method: 'get',
    params: query
  })
}

// 查询个人工作汇报列表
export function myReport(query) {
  return request({
    url: '/oa/report/my',
    method: 'get',
    params: query
  })
}

// 查询待审阅工作汇报列表
export function todoReport(query) {
  return request({
    url: '/oa/report/toto',
    method: 'get',
    params: query
  })
}

// 查询工作汇报详细
export function getReport(id) {
  return request({
    url: '/oa/report/' + id,
    method: 'get'
  })
}

// 新增工作汇报
export function addReport(data) {
  return request({
    url: '/oa/report',
    method: 'post',
    data: data
  })
}

// 修改工作汇报
export function updateReport(data) {
  return request({
    url: '/oa/report',
    method: 'put',
    data: data
  })
}

// 删除工作汇报
export function delReport(id) {
  return request({
    url: '/oa/report/' + id,
    method: 'delete'
  })
}

// 导出工作汇报
export function exportReport(query) {
  return request({
    url: '/oa/report/export',
    method: 'get',
    params: query
  })
}