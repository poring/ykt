import request from '@/utils/request'

// 查询工作流步骤列表
export function listStep(query) {
  return request({
    url: '/oa/step/list',
    method: 'get',
    params: query
  })
}

// 查询工作流步骤详细
export function getStep(id) {
  return request({
    url: '/oa/step/' + id,
    method: 'get'
  })
}

// 新增工作流步骤
export function addStep(data) {
  return request({
    url: '/oa/step',
    method: 'post',
    data: data
  })
}

// 修改工作流步骤
export function updateStep(data) {
  return request({
    url: '/oa/step',
    method: 'put',
    data: data
  })
}

// 删除工作流步骤
export function delStep(id) {
  return request({
    url: '/oa/step/' + id,
    method: 'delete'
  })
}

// 导出工作流步骤
export function exportStep(query) {
  return request({
    url: '/oa/step/export',
    method: 'get',
    params: query
  })
}

// 测试工作流步骤
export function testStep(id) {
  return request({
    url: '/oa/step/test/' + id,
    method: 'get'
  })
}
