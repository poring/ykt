import request from '@/utils/request'

// 查询工作流列表
export function listFlow(query) {
  return request({
    url: '/oa/flow/list',
    method: 'get',
    params: query
  })
}


// 查询工作流列表
export function selectFlow(query) {
  return request({
    url: '/oa/flow/select',
    method: 'get',
    params: query
  })
}

// 查询工作流详细
export function getFlow(id) {
  return request({
    url: '/oa/flow/' + id,
    method: 'get'
  })
}

// 新增工作流
export function addFlow(data) {
  return request({
    url: '/oa/flow',
    method: 'post',
    data: data
  })
}

// 修改工作流
export function updateFlow(data) {
  return request({
    url: '/oa/flow',
    method: 'put',
    data: data
  })
}

// 删除工作流
export function delFlow(id) {
  return request({
    url: '/oa/flow/' + id,
    method: 'delete'
  })
}

// 导出工作流
export function exportFlow(query) {
  return request({
    url: '/oa/flow/export',
    method: 'get',
    params: query
  })
}