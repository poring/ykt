import request from '@/utils/request'

// 查询课程列表
export function listCourse(query) {
  return request({
    url: '/base/course/list',
    method: 'get',
    params: query
  })
}


// 查询课程列表
export function selectCourse(query) {
  return request({
    url: '/base/course/select',
    method: 'get',
    params: query
  })
}

// 查询课程详细
export function getCourse(id) {
  return request({
    url: '/base/course/' + id,
    method: 'get'
  })
}

// 新增课程
export function addCourse(data) {
  return request({
    url: '/base/course',
    method: 'post',
    data: data
  })
}

// 修改课程
export function updateCourse(data) {
  return request({
    url: '/base/course',
    method: 'put',
    data: data
  })
}

// 删除课程
export function delCourse(id) {
  return request({
    url: '/base/course/' + id,
    method: 'delete'
  })
}

// 启用课程
export function enableCourse(id) {
  return request({
    url: '/base/course/enable/' + id,
    method: 'get'
  })
}


// 禁用课程
export function disableCourse(id) {
  return request({
    url: '/base/course/disable/' + id,
    method: 'get'
  })
}

// 导出课程
export function exportCourse(query) {
  return request({
    url: '/base/course/export',
    method: 'get',
    params: query
  })
}

// 下载导入模板
export function importTemplate() {
  return request({
    url: '/base/course/importTemplate',
    method: 'get'
  })
}